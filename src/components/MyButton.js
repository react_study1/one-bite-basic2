const MyButton = ({text, type, onClick}) => {

    // type이 배열 안에 있는 값이면 typed을 반환하고 없으면 default를 반환한다.
    const btnType = ['positive', 'negative'].includes(type) ? type : 'default';

    return (
        <button className={["MyButton", `Mybutton_${btnType}`].join(" ")}
        onClick={onClick}
        >
            {text}
        </button>
    );
};

// type이 입력되지 않으면 default로 한다.
MyButton.defaultProps = {
    type: "default",
}
export default MyButton;